package Home

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	Handlers "gitlab.com/zendrulat123/gow/Handlers/Headers"
)

var tpl *template.Template
var err error

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

//get handler
func Gohome(w http.ResponseWriter, r *http.Request) {
	Handlers.Header(w, r)
	//cc:=Clients.Clientget(Clients.ReadUserIP(), "/go", )

	switch r.Method {
	case "GET":
		err := tpl.ExecuteTemplate(w, "go.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	case "POST":
		err := tpl.ExecuteTemplate(w, "go.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}

}
