package DB

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// The model.
type UserModel struct {
	ID    int    `gorm:"primary_key";"AUTO_INCREMENT"`
	Fname string `gorm:"size:255"`
	Lname string `gorm:"type:varchar(100)”`
	Email string `gorm:"type:varchar(100)”`
	IP    string `gorm:"type:varchar(100)”`
	URLS  string `gorm:"type:varchar(100)”`
}

// The model.
type UserData struct {
	ID   int    `gorm:"primary_key";"AUTO_INCREMENT"`
	IP   string `gorm:"type:varchar(100)”`
	URLS string `gorm:"type:varchar(100)”`
}

type Database struct {
	gd *gorm.DB
}

func (d Database) Open(dialect string, args ...interface{}) (db *gorm.DB, err error)
func (d Database) DB() *gorm.DB {

	db, err := gorm.Open("mysql", "root:@/tes")
	d.gd = db
	d.gd.LogMode(true)
	defer d.gd.Close()
	if err != nil {
		panic("Failed to open the SQLite database.")
	}
	log.Println("Connection Established")
	return d.gd
}

// func (d Database, n string) GetUser(u UserModel, n string) {
// 	dget := d.DB()
// 	dget.Where("name = ?", n).First(&u.Fname)
// }
