package Clients

import (
	"fmt"
	"net/http"
	"net/url"
)

type Clients struct {
	IP    string
	URLS  *url.URL
	Email string
	Name  string
}

var Clientss []Clients

func Clientget(ip string, urls *url.URL, email string, name string) []Clients {
	c := Clients{IP: ip, URLS: urls, Email: email, Name: name}
	cc := append(Clientss, c)
	fmt.Println(cc)
	return cc
}

//ReadUserIP gets ip address.
func ReadUserIP(r *http.Request) string {
	IPAddress := r.Header.Get("X-Real-Ip")
	if IPAddress == "" {
		IPAddress = r.Header.Get("X-Forwarded-For")
	}
	if IPAddress == "" {
		IPAddress = r.RemoteAddr
	}
	return IPAddress
}
