package Email

import (
	"fmt"
	"net/http"
	"net/smtp"
)

// smtpServer data to smtp server
type smtpServer struct {
	host string
	port string
}

// Address URI to smtp server
func (s *smtpServer) Address() string {
	return s.host + ":" + s.port
}
func Email(w http.ResponseWriter, r *http.Request) {
	// Sender data.
	from := "xxxx@gmail.com"
	password := "xxx@"
	// Receiver email address.

	to := []string{
		"snotchy2@gmail.com",
	}
	// smtp server configuration.
	smtpServer := smtpServer{host: "smtp.gmail.com", port: "587"}
	// Message.
	message := []byte("This is a really unimaginative message, I know.")
	// Authentication.
	auth := smtp.PlainAuth("", from, password, smtpServer.host)
	// Sending email.
	err := smtp.SendMail(smtpServer.Address(), auth, from, to, message)
	if err != nil {
		fmt.Println("Email Not Sent!")
		fmt.Println(err)
		return
	}
	fmt.Println("Email Sent!")

}
