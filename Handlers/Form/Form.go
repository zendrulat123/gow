package Home

import (
	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/smtp"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/zendrulat123/gow/Handlers/Clients"
	Handlers "gitlab.com/zendrulat123/gow/Handlers/Headers"
)

// The model.
type UserModel struct {
	ID    int    `gorm:"primary_key";"AUTO_INCREMENT"`
	Fname string `gorm:"size:255"`
	Lname string `gorm:"type:varchar(100)”`
	Email string `gorm:"type:varchar(100)”`
	IP    string `gorm:"type:varchar(100)”`
	URLS  string `gorm:"type:varchar(100)”`
}

// The model.
type UserData struct {
	ID   int    `gorm:"primary_key";"AUTO_INCREMENT"`
	IP   string `gorm:"type:varchar(100)”`
	URLS string `gorm:"type:varchar(100)”`
}

var tpl *template.Template
var err error

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

//get handler
func Form(w http.ResponseWriter, r *http.Request) {
	Handlers.Header(w, r)
	switch r.Method {
	case "GET":
		err := tpl.ExecuteTemplate(w, "form.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}
	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}

}

// smtpServer data to smtp server
type smtpServer struct {
	host string
	port string
}

// Address URI to smtp server
func (s *smtpServer) Address() string {
	return s.host + ":" + s.port
}

//formss handler
func Formss(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case "GET":
		err := tpl.ExecuteTemplate(w, "form.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}
	case "POST":
		//get data from form
		r.ParseForm()

		queryMap := r.Form

		fname := r.FormValue("fname")
		lname := r.FormValue("lname")
		email := r.FormValue("email")
		userstemp := UserModel{Fname: fname, Lname: lname, Email: email}
		fmt.Println(fname, lname, queryMap)
		//database beginsssssss
		db, err := gorm.Open("mysql", "root:@/test")
		db.LogMode(true)
		defer db.Close()
		if err != nil {
			panic("Failed to open the SQLite database.")
		}

		log.Println("Connection Established")
		db.HasTable("user_data")

		db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&UserModel{})
		db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&UserData{})
		db.Debug().AutoMigrate(&UserModel{})
		db.Debug().AutoMigrate(&UserData{})
		c := context.Background()
		cip := Clients.ReadUserIP(r)

		// cc := Clients.Clients{IP: cip, URLS: f, Email: email, Name: fname}
		// fmt.Println(cc)
		//database creation
		var users []UserModel = []UserModel{
			UserModel{Fname: fname, Lname: lname, Email: email, IP: cip, URLS: intervert(r, c)},
		}
		var userdatas []UserData = []UserData{
			UserData{IP: cip, URLS: intervert(r, c)},
		}
		for _, user := range users {
			db.Create(&user)
		}
		for _, userdatass := range userdatas {
			db.Create(&userdatass)
		}

		//db.DropTableIfExists(&users, "user_models")
		//#Cookie
		// var hashKey = []byte("very-secret")
		// var s = securecookie.New(hashKey, nil)
		// encoded, err := s.Encode(fname, email)

		// if err == nil {
		// 	cookie := &http.Cookie{
		// 		Name:  fname,
		// 		Value: encoded,
		// 		Path:  "/",
		// 	}
		// 	http.SetCookie(w, cookie)
		// 	ctx := context.WithValue(r.Context(), cookie.Name, cookie.Value)
		// 	fmt.Println(ctx)
		// } else {
		// 	//Add data to context
		// 	fmt.Println("cookie not set")
		// }

		//beginning of sending email
		// Sender data.
		from := "xx@gmail.com"
		password := "xxx@"
		// Receiver email address.

		to := []string{
			email,
		}
		// smtp server configuration.
		smtpServer := smtpServer{host: "smtp.gmail.com", port: "587"}
		// Message.
		message := []byte(`Thank you " + fname + " for submitting your email.  I hope you enjoy my site.  
		If you need any resources here is a link <a href=\"localhost/go"></a>`)
		// Authentication.
		auth := smtp.PlainAuth("", from, password, smtpServer.host)
		// Sending email.
		err = smtp.SendMail(smtpServer.Address(), auth, from, to, message)
		if err != nil {
			fmt.Println("Email Not Sent!")
			fmt.Println(err)
			return
		}
		fmt.Println("Email Sent!")

		fmt.Println(userstemp)
		http.Redirect(w, r, "/go", 301)
		// Other HTTP methods (eg PUT, PATCH, etc) are not handled by the above
		// so inform the client with appropriate status code
		w.WriteHeader(http.StatusMethodNotAllowed)

	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")

	}
}
func intervert(r *http.Request, c context.Context) string {
	ccc := r.WithContext(c).URL
	cstr := fmt.Sprintf("%v", ccc)
	return cstr
}
