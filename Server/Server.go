package Server

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"time"

	//only 3rd parties
	. "github.com/logrusorgru/aurora"
	"github.com/rs/cors"

	//basically just handlers
	Email "gitlab.com/zendrulat123/gow/Handlers/Email"
	Filestructure "gitlab.com/zendrulat123/gow/Handlers/Filestructure"
	Formss "gitlab.com/zendrulat123/gow/Handlers/Form"
	Gohome "gitlab.com/zendrulat123/gow/Handlers/Go-home"
	Goroutines "gitlab.com/zendrulat123/gow/Handlers/Goroutines"
	Goserver "gitlab.com/zendrulat123/gow/Handlers/Goserver"
	Imports "gitlab.com/zendrulat123/gow/Handlers/Imports"
	Iuse "gitlab.com/zendrulat123/gow/Handlers/Iuse"
	Middle "gitlab.com/zendrulat123/gow/Handlers/Middle"
	Scaling "gitlab.com/zendrulat123/gow/Handlers/Scaling"
	Types "gitlab.com/zendrulat123/gow/Handlers/Types"
	Welcome "gitlab.com/zendrulat123/gow/Handlers/Welcome"
)

var err error

//used for printing time of request
var Start = time.Now()
var Durations = time.Now().Sub(Start)

//getting context
type Contexter struct {
	M      string
	U      *url.URL
	P      string
	B      io.ReadCloser
	Gb     func() (io.ReadCloser, error)
	Host   string
	Form   url.Values
	Cancel <-chan struct{}
	R      *http.Response
	H      http.Header
	D      time.Duration
	I      string
}

//used to shorten use of Contexter
var CC Contexter

//initializing context
func AddContext(ctx context.Context, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		Start := time.Now()
		Duration := time.Now().Sub(Start)

		CC = Contexter{
			M:      r.WithContext(ctx).Method,
			U:      r.WithContext(ctx).URL,
			P:      r.WithContext(ctx).Proto,
			B:      r.WithContext(ctx).Body,
			Host:   r.WithContext(ctx).Host,
			Form:   r.WithContext(ctx).Form,
			Cancel: r.WithContext(ctx).Cancel,
			R:      r.WithContext(ctx).Response,
			D:      Duration,
			H:      r.WithContext(ctx).Header,
			//I:      Clients.ReadUserIP(r),
		}

		fmt.Println(Blue("/ʕ◔ϖ◔ʔ/````````````````````````````````````````````"))
		fmt.Printf("Method:%s\n - Status:%s\n - URL:%s - Body:%v\n - Host:%s\n - Form:%v\n - Cancel:%d\n - Response:%d\n - Dur:%02d-00:00\n - Cache-Control:%s - Accept:%s\n - IP:%s\n",
			Cyan(CC.M),
			Brown(r.Header.Get("X-Forwarded-Port")),
			Red(CC.U),
			Blue(CC.B),
			Yellow(CC.Host),
			BgRed(CC.Form),
			BgGreen(CC.Cancel),
			BgBrown(CC.R),
			BgMagenta(CC.D),
			Red(CC.H.Get("Cache-Control")),
			Blue(CC.H.Get("Accept")),
			Yellow(CC.I),
		)
		//this is to spit out the ctx.header in pieces cause its exhaustive
		// for k, v := range CC.H {
		// 	fmt.Println("\n", k, v)
		// }
		cookie, _ := r.Cookie("username")

		if cookie != nil {
			//Add data to context
			ctx := context.WithValue(r.Context(), "Username", cookie.Value)
			next.ServeHTTP(w, r.WithContext(ctx))

		} else {

			if err != nil {
				// Error occurred while parsing request body
				w.WriteHeader(http.StatusBadRequest)

				return
			}
			next.ServeHTTP(w, r.WithContext(ctx))
		}
	})
}

//starts the server
func Serv() {

	mux := http.NewServeMux() //used for cors
	//just handlers
	mux.HandleFunc("/", Gohome.Gohome)
	mux.HandleFunc("/go", Gohome.Gohome)
	mux.HandleFunc("/types", Types.Types)
	mux.HandleFunc("/server", Goserver.Goserver)
	mux.HandleFunc("/middle", Middle.Middle)
	mux.HandleFunc("/filestructure", Filestructure.Filestructure)
	mux.HandleFunc("/goroutines", Goroutines.Goroutines)
	mux.HandleFunc("/iuse", Iuse.Iuse)
	mux.HandleFunc("/scaling", Scaling.Scaling)
	mux.HandleFunc("/email", Email.Email)
	mux.HandleFunc("/form", Formss.Form)
	mux.HandleFunc("/formss", Formss.Formss)
	mux.HandleFunc("/imports", Imports.Imports)
	mux.HandleFunc("/welcome", Welcome.Welcome)
	//serving images/css
	fs := http.FileServer(http.Dir("static/"))
	mux.Handle("/static/", http.StripPrefix("/static/", fs))
	//cors/context logger
	handler := cors.Default().Handler(mux)
	c := context.Background()
	log.Fatal(http.ListenAndServe(":8081", AddContext(c, handler)))

}
