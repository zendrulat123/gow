module gitlab.com/zendrulat123/gow

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.4 // indirect
	github.com/gorilla/securecookie v1.1.1
	github.com/jinzhu/gorm v1.9.12
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/rs/cors v1.7.0
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
)
